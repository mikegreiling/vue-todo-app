var config = {
  entry: './app.js',
  output: {
    filename: 'app.bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }]
  },
  devServer: {
    port: 8081
  }
};

module.exports = config;
