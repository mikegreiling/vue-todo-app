import Vue from 'vue/dist/vue';
import Todo from './Todo';

const vm = new Vue({
  el: '#app',
  render: h => h(Todo),
});
