# GitLab `.jsx` vs `.vue` template research project

In an effort to better understand the pros and cons of adopting `.jsx` versus
`.vue` template files I decided to create a simple VueJS app and then attempt to
port it to each.

The app is a moderately sophisticated "todo list" app based largely on
<https://codepen.io/simonswiss/pen/vKQzEq>.

## Testing

The original `<script type="text/x-template" />`-style implementation is in the
`master` branch.  The `.vue` conversion is in the `convert-to-dot-vue` branch.
And finally, the `.jsx` conversion is in the `convert-to-jsx` branch.

To test any of these, simply checkout a branch, run `yarn install` then run
`yarn dev`.  You should be able to open your browser to <http://localhost:8081/>
and see the app in action.

## Conclusions

### 1. Bundle size

For minimal bundle-size, JSX wins by a hair.

* **master** _93KB_
* **convert-to-dot-vue** _57.7KB_
* **convert-to-jsx** _56.3KB_

This isn't a gigantic savings, so I'd hesitate to call this much of an advantage.

### 2. Ease of use

Vue templates win by a long shot.  I really didn't expect the conversion to JSX
to be such a pain point, but the tooling just isn't very mature yet.  Everything
is much more verbose.  I knew going into it that `v-if`, `v-show`, and `v-for`
would need to be replaced with javascript ternary logic or `Array.map` methods,
but I hadn't realized that niceties like
[v-model](https://vuejs.org/v2/guide/render-function.html#v-model) and event
binding would require a _lot_ more boilerplate.

_example_:

**vue template**
```html
<input
  type="text"
  @keyup.enter="addTodo"
  v-model="newTodo"
  placeholder="Type and press enter..."
/>
```

**JSX**
```html
<input
  type="text"
  onKeyup={this.addTodo}
  onInput={e => this.newTodo = e.target.value}
  domPropsValue={this.newTodo}
  placeholder="Type and press enter..."
/>
```

^ also in the JSX version `this.addTodo` needed to have extra code inserted to
detect the `"enter"` key from the event argument.

### 3. Conclusions

I was really hoping for JSX to work out, but after working on this I think we're
making the right decision here to go with `.vue` templates.  Any benefits we'd
gain by having community members who come in with JSX experience from the React
world would be completely negated by having to learn and cope with a [very
different API](https://github.com/vuejs/babel-plugin-transform-vue-jsx#difference-from-react-jsx)
(`class` not `className`, `domPropsValue` not `value`, ...).  I don't think the
React experience would translate as well as I had initially hoped.
