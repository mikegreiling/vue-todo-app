export default {
  template: '#app-template',

  data() {
    return {
      newTodo: '',
      todos: [
        {text: 'drink coffee', completed: true},
        {text: 'check e-mails', completed: false},
        {text: 'hang in slack', completed: false},
        {text: 'do some work', completed: false},
      ]
    };
  },

  computed: {
    itemsDone() {
      return this.todos.filter(todo => todo.completed);
    },

    itemsTodo() {
      return this.todos.filter(todo => !todo.completed);
    }
  },

  methods: {
    addTodo() {
      if (!this.newTodo.length) return;

      this.todos.push({
        text: this.newTodo,
        completed: false
      });
      this.newTodo = '';
    },

    removeTodo(index) {
      this.todos.splice(index, 1)
    },

    clearCompleted() {
      this.todos = this.itemsTodo;
    }
  }
};
